var Report = cc.LayerColor.extend({
	init: function() {
		this.setPosition(new cc.Point(0, 0));
		this.backGround = new cc.Sprite.create("res/KL_Report.png");
		this.backGround.setPosition(new cc.Point(400, 300));
		this.addChild(this.backGround);

		var income = money - beforeMoney;

		this.cupSoldLabel = cc.LabelTTF.create(cupSold.toString(),"AG Book Rounded",30);
		this.cupSoldLabel.setPosition(new cc.Point(400, 380));
		this.addChild(this.cupSoldLabel);

		this.incomeLabel = cc.LabelTTF.create(income.toFixed(1).toString(),"AG Book Rounded",30);
		this.incomeLabel.setPosition(new cc.Point(400, 305));
		this.addChild(this.incomeLabel);

		this.expenseLabel = cc.LabelTTF.create(expense.toFixed(1).toString(),"AG Book Rounded",30);
		this.expenseLabel.setPosition(new cc.Point(400, 225));
		this.addChild(this.expenseLabel);

		this.profitLabel = cc.LabelTTF.create((income - expense).toFixed(1).toString(),"AG Book Rounded",30);
		this.profitLabel.setPosition(new cc.Point(400, 150));
		this.addChild(this.profitLabel);

		this.btnGo = new BtnGo();
		this.btnGo.setPosition(new cc.Point(700, 100));
		this.btnGo.addNextReportEvent(this);
		this.addChild(this.btnGo);

		return true;
	}
});