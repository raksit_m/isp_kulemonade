var Instruction = cc.LayerColor.extend({
	init: function() {
		this.setPosition(new cc.Point(0, 0));

		this.backGround = new cc.Sprite.create("res/KL_Instruction1.png");
		this.backGround.setPosition(new cc.Point(400, 300));
		this.addChild(this.backGround, 0);

		this.createNextPageButton();

		this.createPreviousPageButton();

		this.introBG = [];
		this.addAllBackground();

		this.currentIntro = 1;

		this.addNextIntroEvent(this);
		this.addBackIntroEvent(this);

		this.scheduleUpdate();

		return true;
	},

	addAllBackground : function() {
		for(var i = 1; i <= 24; i++) {
			var instructionImage = new cc.Sprite.create("res/KL_Instruction" + i + ".png");
			instructionImage.setPosition(new cc.Point(400, 300));
			this.introBG.push(instructionImage);
		}
	},

	addNextIntroEvent : function(self) {
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseUp: function(event) {
				if(event.getLocationX() > 680 && event.getLocationX() < 730 
					&& event.getLocationY() > 80 && event.getLocationY() < 120) {
					if(self.currentIntro == 24) {
						var firstPage = new FirstPage();
						firstPage.init();
						self.getParent().addChild(firstPage);
						self.getParent().removeChild(self);
						return;
					}

					self.currentIntro++;
					self.changePage();
				}
			}
		}, this);
	},

	addBackIntroEvent : function(self) {
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseUp: function(event) {
				if(event.getLocationX() > 45 && event.getLocationX() < 90 
					&& event.getLocationY() > 80 && event.getLocationY() < 120 && self.currentIntro != 1) {
					self.currentIntro--;
					self.changePage();
				}
			}
		}, this);
	},

	createNextPageButton : function() {
		this.btnGo = new BtnGo();
		this.btnGo.setPosition(new cc.Point(700, 100));
		this.addChild(this.btnGo, 1);
	},

	createPreviousPageButton : function() {
		this.btnBack = new BtnBack();
		this.btnBack.setPosition(new cc.Point(70, 100));
		this.addChild(this.btnBack, 2);
	},

	changePage : function() {
		this.removeChild(this.backGround);
		this.backGround = this.introBG[this.currentIntro-1];
		this.backGround.setPosition(new cc.Point(400, 300));
		this.addChild(this.backGround, 0);
	},

	update : function() {
		if(this.currentIntro == 1) {
			this.btnBack.setVisible(false);
		}
		else {
			this.btnBack.setVisible(true);
		}
	}


});