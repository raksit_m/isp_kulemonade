var BtnInstruction = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile('res/KL_Instructions_Button.png');
	},

	addBtnInstructionEvent : function(self) {
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseUp: function(event) {
				if(event.getLocationX() > 0 && event.getLocationX() < 300 
					&& event.getLocationY() > 110 && event.getLocationY() < 210) {
						var instruction = new Instruction();
						instruction.init();
						self.getParent().addChild(instruction);
						self.getParent().removeChild(self);
					}
				}
		}, this);
	}
});