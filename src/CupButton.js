var CupButton = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile("res/KL_Cup_Button.png");
	},

	addBtnCupEvent : function(self) {
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseUp: function(event) {
				if(event.getLocationX() > 660 && event.getLocationX() < 730 
					&& event.getLocationY() > 250 && event.getLocationY() < 420) {
					var cupShop = new CupShop();
					cupShop.init();
					self.getParent().addChild(cupShop);
					self.getParent().removeChild(self);
				}
			}
		}, this);
	}
});