var Customer = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile("res/KL_Customer.png");
	},

	moveForward: function() {
		var pos = this.getPosition();
		this.setPosition(new cc.Point(pos.x + 10, pos.y));
	}
});