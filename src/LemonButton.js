var LemonButton = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile("res/KL_Lemon_Button.png");
	},

	addBtnLemonEvent : function(self) {
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseUp: function(event) {
				if(event.getLocationX() > 60 && event.getLocationX() < 190 
					&& event.getLocationY() > 250 && event.getLocationY() < 420) {
					var lemonShop = new LemonShop();
					lemonShop.init();
					self.getParent().addChild(lemonShop);
					self.getParent().removeChild(self);
				}
			}
		}, this);
	}
});