var BtnGo = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile("res/KL_Go.png");
	},

	addStartDayEvent : function(self) {
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseUp: function(event) {
				if(event.getLocationX() > 680 && event.getLocationX() < 730 
					&& event.getLocationY() > 80 && event.getLocationY() < 120) {
					if(lemon == 0 || ice == 0 || sugar == 0 || cup ==0) {
						var popup2 = new Popup2();
						popup2.init();
						self.getParent().addChild(popup2);
						return;
					}
					day += 1;
					beforeMoney = money;
					var monitor = new Interface();
					monitor.init();
					self.getParent().addChild(monitor);
					self.getParent().removeChild(self);
				}
			}
		}, this);
	},

	addEndDayEvent : function(self) {
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseUp: function(event) {
				if(event.getLocationX() > 680 && event.getLocationX() < 730 
					&& event.getLocationY() > 80 && event.getLocationY() < 120) {
					var report = new Report();
					report.init();
					self.getParent().addChild(report);
					self.getParent().removeChild(self);
				}
			}
		}, this);
	},

	addNextReportEvent : function(self) {
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseUp: function(event) {
				if(event.getLocationX() > 680 && event.getLocationX() < 730 
					&& event.getLocationY() > 80 && event.getLocationY() < 120) {
					var report2 = new Report2();
					report2.init();
					self.getParent().addChild(report2);
					self.getParent().removeChild(self);
				}
			}
		}, this);
	},

	addNextDayEvent : function(self) {
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseUp: function(event) {
				if(event.getLocationX() > 680 && event.getLocationX() < 730 
					&& event.getLocationY() > 80 && event.getLocationY() < 120) {
					money -= expense;
					if(money < 0) {
						this.bankrupted = cc.Sprite.create("res/KL_Bankrupted.png");
						this.bankrupted.setPosition(new cc.Point(400, 300));
						self.getParent().addChild(this.bankrupted);
					}

					else {
						var firstPage = new FirstPage();
						firstPage.init();
						self.getParent().addChild(firstPage);
						goodPerformance = 0;
						badPerformance = 0;
						expensivePerformance = 0;
					}

					self.getParent().removeChild(self);
				}
			}
		}, this);
	}
});