var SugarButton = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile("res/KL_Sugar_Button.png");
	},

	addBtnSugarEvent : function(self) {
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseUp: function(event) {
				if(event.getLocationX() > 430 && event.getLocationX() < 560 
					&& event.getLocationY() > 250 && event.getLocationY() < 420) {
					var sugarShop = new SugarShop();
					sugarShop.init();
					self.getParent().addChild(sugarShop);
					self.getParent().removeChild(self);
				}
			}
		}, this);
	}
});