var IceButton = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile("res/KL_Ice_Button.png");
	},

	addBtnIceEvent : function(self) {
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseUp: function(event) {
				if(event.getLocationX() > 240 && event.getLocationX() < 370 
					&& event.getLocationY() > 250 && event.getLocationY() < 420) {
					var iceShop = new IceShop();
					iceShop.init();
					self.getParent().addChild(iceShop);
					self.getParent().removeChild(self);
				}
			}
		}, this);
	}
});