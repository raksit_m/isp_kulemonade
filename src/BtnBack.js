var BtnBack = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile("res/KL_Back.png");
	},

	addPopupEvent : function(self) {
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseUp: function(event) {
				if(event.getLocationX() > 45 && event.getLocationX() < 90 
					&& event.getLocationY() > 500 && event.getLocationY() < 540) {
					self.getParent().removeChild(self);
				}
			}
		}, this);
	}
});