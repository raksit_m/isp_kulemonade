var Interface = cc.LayerColor.extend({
	init: function() {
		this.setPosition(new cc.Point(0, 0));
		
		this.backGround = new cc.Sprite.create('res/KL_DemoUI.png');
		this.backGround.setPosition(new cc.Point(400, 300));
		this.addChild(this.backGround);

		this.createStockView();

		this.createDayView();

		this.createMoneyLabel();

		this.weather = parseInt(Math.floor((Math.random()*4)+1));

		this.createWeatherIcon();

		this.createPerformanceView();

		this.points = [];
		this.createCustomers();

		this.scheduleUpdate();
	},

	createStockView : function() {
		this.amountLemon = cc.LabelTTF.create(lemon.toString(),"AG Book Rounded",40);
		this.amountLemon.setPosition(new cc.Point(350, 110));
		this.addChild(this.amountLemon);

		this.amountIce = cc.LabelTTF.create(ice.toString(),"AG Book Rounded",40);
		this.amountIce.setPosition(new cc.Point(350, 40));
		this.addChild(this.amountIce);

		this.amountSugar = cc.LabelTTF.create(sugar.toString(),"AG Book Rounded",40);
		this.amountSugar.setPosition(new cc.Point(580, 110));
		this.addChild(this.amountSugar);

		this.amountCup = cc.LabelTTF.create(cup.toString(),"AG Book Rounded",40);
		this.amountCup.setPosition(new cc.Point(580, 40));
		this.addChild(this.amountCup);
	},

	createDayView : function() {
		this.days = cc.LabelTTF.create(day.toString(),"AG Book Rounded",40);
		this.days.setPosition(new cc.Point(200, 545));
		this.addChild(this.days);
	},

	createMoneyLabel : function() {
		this.moneyLabel = cc.LabelTTF.create(money.toFixed(1).toString() + " $","AG Book Rounded",40);
		this.moneyLabel.setPosition(new cc.Point(550, 545));
		this.addChild(this.moneyLabel);
	},

	createWeatherIcon : function() {
		this.weatherIcon = cc.Sprite.create("res/KL_Weather" + this.weather + ".png");
		this.weatherIcon.setPosition(new cc.Point(700, 550));
		this.addChild(this.weatherIcon);
	},

	createPerformanceView : function() {
		this.goodEmoticon = cc.Sprite.create("res/KL_Good.png");
		this.goodEmoticon.setPosition(new cc.Point(600, 420));
		this.addChild(this.goodEmoticon);

		this.goodLabel = cc.LabelTTF.create(goodPerformance.toString(),"AG Book Rounded",40);
		this.goodLabel.setPosition(new cc.Point(700, 415));
		this.addChild(this.goodLabel);

		this.badEmoticon = cc.Sprite.create("res/KL_Bad.png");
		this.badEmoticon.setPosition(new cc.Point(600, 340));
		this.addChild(this.badEmoticon);

		this.badLabel = cc.LabelTTF.create(badPerformance.toString(),"AG Book Rounded",40);
		this.badLabel.setPosition(new cc.Point(700, 335));
		this.addChild(this.badLabel);

		this.expensiveEmoticon = cc.Sprite.create("res/KL_Expensive.png");
		this.expensiveEmoticon.setPosition(new cc.Point(600, 260));
		this.addChild(this.expensiveEmoticon);

		this.expensiveLabel = cc.LabelTTF.create(expensivePerformance.toString(),"AG Book Rounded",40);
		this.expensiveLabel.setPosition(new cc.Point(700, 255));
		this.addChild(this.expensiveLabel);
	},

	update: function() {

		if(this.stockRunOut()) {
			this.endDay();
		}

		for(var i = 0; i < 5; i++) {
			this.points[i].moveForward();
			var pos = this.points[i].getPosition();
			if(pos.x >= 400) {
				this.sell();

				this.realPrice = pricePerCup;
				this.performance = Interface.PERFORMANCE.GOOD;

				this.checkRecipeWeather();

				this.checkPricePerCup();

				this.checkCustomerTaste();

				money += this.realPrice;

				this.points[i].setPosition(new cc.Point(-100, 300));
				this.updatePerformance();
			}
		}

		this.updateStockView();

		this.updatePerformanceView();
	},

	stockRunOut : function() {
		return (lemon < lemonPerCup || ice < icePerCup || sugar < sugarPerCup || cup <= 0);
	},

	endDay : function() {
		this.unscheduleUpdate();
		this.btnGo = new BtnGo();
		this.btnGo.setPosition(new cc.Point(700, 100));
		this.btnGo.addEndDayEvent(this);
		this.addChild(this.btnGo);
		return;
	},

	sell : function() {
		lemon -= lemonPerCup;
		ice -= icePerCup;
		sugar -= sugarPerCup;
		cup -= 1;
		cupSold += 1;
	},

	checkRecipeWeather : function() {
		if(this.weather == Interface.WEATHER.SUNNY && icePerCup < 3) {
			this.realPrice /= (4 - icePerCup);
			this.performance = Interface.PERFORMANCE.BAD;
		}

		if(this.weather == Interface.WEATHER.RAIN && icePerCup >= 3) {
			this.realPrice /= icePerCup;
			this.performance = Interface.PERFORMANCE.BAD;
		}
	},

	checkPricePerCup : function() {
		if(pricePerCup >= 1.5 && parseInt(Math.floor((Math.random()*3)+1)) == 1) {
			this.realPrice /= 2;
			this.performance = Interface.PERFORMANCE.EXPENSIVE;
		}				
	},

	checkCustomerTaste : function() {
		if(parseInt(Math.floor((Math.random()*5))) > 3) {
			if(this.performance == Interface.PERFORMANCE.GOOD || lemonPerCup == 1) {
				this.performance = Interface.PERFORMANCE.BAD;
				this.realPrice /= 2;
			}

		    else if(this.performance == Interface.PERFORMANCE.BAD) {
				this.performance = Interface.PERFORMANCE.GOOD;
				this.realPrice = pricePerCup;
			}
		}
	},

	updateStockView : function() {
		this.amountLemon.setString(lemon.toString());
		this.amountIce.setString(ice.toString());
		this.amountSugar.setString(sugar.toString());
		this.amountCup.setString(cup.toString());
		this.moneyLabel.setString(money.toFixed(1).toString() + " $");
	},

	updatePerformanceView : function() {
		this.goodLabel.setString(goodPerformance.toString());
		this.badLabel.setString(badPerformance.toString());
		this.expensiveLabel.setString(expensivePerformance.toString());
	},

	createCustomers: function() {
		for(var i = 0; i < 5; i++) {
			var customer = new Customer();
			customer.setPosition(new cc.Point(100*i, 300));
			this.points.push(customer);
			this.addChild(this.points[i]);
		}
	},

	updatePerformance: function() {
		if(this.performance == Interface.PERFORMANCE.GOOD) {
			goodPerformance += 1;
		}

		else if(this.performance == Interface.PERFORMANCE.BAD) {
			badPerformance += 1;
		}

		else if(this.performance == Interface.PERFORMANCE.EXPENSIVE) {
			expensivePerformance += 1;
		}

		this.performance = Interface.PERFORMANCE.GOOD;
	}
});

Interface.WEATHER = {
	SUNNY : 1,
	PARTLY_CLOUDY : 2,
	CLOUDY : 3,
	RAIN : 4
}

Interface.PERFORMANCE = {
	GOOD : 1,
	BAD : 2,
	EXPENSIVE : 3
}