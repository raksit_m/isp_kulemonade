var Report2 = cc.LayerColor.extend({
	init: function() {
		this.setPosition(new cc.Point(0, 0));
		this.backGround = new cc.Sprite.create("res/KL_Report2.png");
		this.backGround.setPosition(new cc.Point(400, 300));
		this.addChild(this.backGround);

		this.goodLabel = cc.LabelTTF.create(goodPerformance.toString(),"AG Book Rounded",40);
		this.goodLabel.setPosition(new cc.Point(600, 395));
		this.addChild(this.goodLabel);

		this.badLabel = cc.LabelTTF.create(badPerformance.toString(),"AG Book Rounded",40);
		this.badLabel.setPosition(new cc.Point(600, 305));
		this.addChild(this.badLabel);

		this.expensiveLabel = cc.LabelTTF.create(expensivePerformance.toString(),"AG Book Rounded",40);
		this.expensiveLabel.setPosition(new cc.Point(600, 215));
		this.addChild(this.expensiveLabel);

		this.checkLemonSpoiled();

		this.checkIceMelted();

		this.btnGo = new BtnGo();
		this.btnGo.setPosition(new cc.Point(700, 100));
		this.btnGo.addNextDayEvent(this);
		this.addChild(this.btnGo);

		return true;
	},

	checkLemonSpoiled : function() {
		if(lemon > 0  && parseInt(Math.floor((Math.random()*5))) > 2) {
			var lemonSpoiled = parseInt(Math.floor((Math.random()*lemon)) + 1);
			this.lemonSpoiledLabel = cc.LabelTTF.create("- " + lemonSpoiled + " lemons are spoiled.","AG Book Rounded",30);
			this.lemonSpoiledLabel.setPosition(new cc.Point(400, 140));
			this.addChild(this.lemonSpoiledLabel);
			lemon -= lemonSpoiled;
		}
	},

	checkIceMelted : function() {
		if(ice > 0) {
			this.iceMeltLabel = cc.LabelTTF.create("- " + ice.toString() + " ice is melted.","AG Book Rounded",30);
			this.iceMeltLabel.setPosition(new cc.Point(400, 80));
			this.addChild(this.iceMeltLabel);
			ice = 0;
		}
	},


});