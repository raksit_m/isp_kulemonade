var BtnRecipe = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile('res/KL_Recipe_Button.png');
	},

	addBtnRecipeEvent : function(self) {
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseUp: function(event) {
				if(event.getLocationX() > 0 && event.getLocationX() < 300 
					&& event.getLocationY() > 255 && event.getLocationY() < 355) {
						var recipe = new Recipe();
						recipe.init();
						self.getParent().addChild(recipe);
						self.getParent().removeChild(self);
					}
				}
		}, this);
	}
});