var Popup = cc.LayerColor.extend({
	init: function() {
		this.setPosition(new cc.Point(0, 0));
		this.backGround = new cc.Sprite.create("res/KL_Popup.png");
		this.backGround.setPosition(new cc.Point(400, 300));
		this.addChild(this.backGround);

		this.btnBack = new BtnBack();
		this.btnBack.setPosition(new cc.Point(70, 540));
		this.btnBack.addPopupEvent(this);
		this.addChild(this.btnBack);

		return true;
	}
});