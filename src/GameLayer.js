var GameLayer = cc.LayerColor.extend({
	init: function() {
		this.setPosition(new cc.Point(0, 0));

		this.backGround = new cc.Sprite.create("res/KL_MainMenu.png");
		this.backGround.setPosition(new cc.Point(400, 300));
		this.addChild(this.backGround);

		this.isEnter = false;
		cc.audioEngine.playMusic( "res/KL_Music.mp3", true );

		this.addKeyboardHandlers();

		return true;
	},

	addKeyboardHandlers: function() {
		var self = this;
		cc.eventManager.addListener({
			event: cc.EventListener.KEYBOARD,
			onKeyPressed: function(keyCode, event) {
				if(!this.isEnter) {
					var firstPage = new FirstPage();
					firstPage.init();
					self.removeChild(self.backGround);
					self.addChild(firstPage);
					this.isEnter = true;
				}
			}
		}, this);
	}

});

var StartScene = cc.Scene.extend({
		onEnter: function() {
			this._super();
			var layer = new GameLayer();
			layer.init();
			this.addChild(layer);
		}
});

