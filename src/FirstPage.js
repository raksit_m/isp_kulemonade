var FirstPage = cc.LayerColor.extend({
	init: function() {
		this.setPosition(new cc.Point(0, 0));

		this.backGround = new cc.Sprite.create("res/KL_FirstPage.png");
		this.backGround.setPosition(new cc.Point(400, 300));
		this.addChild(this.backGround);

		this.createStockButton();

		this.createRecipeButton();

		this.createInstructionButton();

		this.createMoneyLabel();

		this.createGoButton();

		this.scheduleUpdate();

		return true;
	},

	createStockButton : function() {
		this.btnStock = new BtnStock();
		this.btnStock.setPosition(new cc.Point(150, 450));
		this.btnStock.addBtnStockEvent(this);
		this.addChild(this.btnStock);
	},

	createRecipeButton : function() {
		this.btnRecipe = new BtnRecipe();
		this.btnRecipe.setPosition(new cc.Point(150, 300));
		this.btnRecipe.addBtnRecipeEvent(this);
		this.addChild(this.btnRecipe);
	},

	createInstructionButton : function() {
		this.btnInstruction = new BtnInstruction();
		this.btnInstruction.setPosition(new cc.Point(150, 150));
		this.btnInstruction.addBtnInstructionEvent(this);
		this.addChild(this.btnInstruction);
	},

	createGoButton : function() {
		this.btnGo = new BtnGo();
		this.btnGo.setPosition(new cc.Point(700, 100));
		this.btnGo.addStartDayEvent(this);
		this.addChild(this.btnGo);
	},

	createMoneyLabel : function() {
		this.moneyLabel = cc.LabelTTF.create(money.toFixed(1).toString() + " $","AG Book Rounded",40);
		this.moneyLabel.setPosition(new cc.Point(590, 250));
		this.addChild(this.moneyLabel);
	},

	update: function() {
		this.moneyLabel.setString(money.toFixed(1).toString() + " $");
	}
});