var Stock = cc.LayerColor.extend({
	init: function() {
		this.setPosition(new cc.Point(0, 0));
		this.backGround = new cc.Sprite.create("res/KL_Stock.png");
		this.backGround.setPosition(new cc.Point(400, 300));
		this.addChild(this.backGround);

		this.btnLemon = new LemonButton();
		this.btnLemon.setPosition(new cc.Point(124, 337));
		this.btnLemon.addBtnLemonEvent(this);
		this.addChild(this.btnLemon);
		this.amountLemon = cc.LabelTTF.create(lemon.toString(),"AG Book Rounded",40);
		this.amountLemon.setPosition(new cc.Point(122, 275));
		this.addChild(this.amountLemon);

		this.btnIce = new IceButton();
		this.btnIce.setPosition(new cc.Point(305, 337));
		this.btnIce.addBtnIceEvent(this);
		this.addChild(this.btnIce);
		this.amountIceCube = cc.LabelTTF.create(ice.toString(),"AG Book Rounded",40);
		this.amountIceCube.setPosition(new cc.Point(303, 275));
		this.addChild(this.amountIceCube);

		this.btnSugar = new SugarButton();
		this.btnSugar.setPosition(new cc.Point(488, 337));
		this.btnSugar.addBtnSugarEvent(this);
		this.addChild(this.btnSugar);
		this.amountSugar = cc.LabelTTF.create(sugar.toString(),"AG Book Rounded",40);
		this.amountSugar.setPosition(new cc.Point(490, 275));
		this.addChild(this.amountSugar);

		this.btnCup = new CupButton();
		this.btnCup.setPosition(new cc.Point(669, 337));
		this.btnCup.addBtnCupEvent(this);
		this.addChild(this.btnCup);
		this.amountCup = cc.LabelTTF.create(cup.toString(),"AG Book Rounded",40);
		this.amountCup.setPosition(new cc.Point(670, 275));
		this.addChild(this.amountCup);

		this.moneyLabel = cc.LabelTTF.create(money.toFixed(1).toString() + " $","AG Book Rounded",40);
		this.moneyLabel.setPosition(new cc.Point(700, 550));
		this.addChild(this.moneyLabel);

		this.btnBack = new BtnBack();
		this.btnBack.setPosition(new cc.Point(70, 540));
		this.addChild(this.btnBack);

		this.addBtnBackEvent(this);
		return true;
	},

	addBtnBackEvent : function(self) {
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseUp: function(event) {
				if(event.getLocationX() > 45 && event.getLocationX() < 90 
					&& event.getLocationY() > 500 && event.getLocationY() < 540) {
					var firstPage = new FirstPage();
					firstPage.init();
					self.getParent().addChild(firstPage);
					self.getParent().removeChild(self);
				}
			}
		}, this);
	}
});	