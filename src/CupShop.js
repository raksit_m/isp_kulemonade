var CupShop = cc.LayerColor.extend({
	init: function() {
		this.setPosition(new cc.Point(0, 0));
		
		this.backGround = new cc.Sprite.create("res/KL_CupShop.png");
		this.backGround.setPosition(new cc.Point(400, 300));
		this.addChild(this.backGround);

		this.createMoneyLabel();
	
		this.createBackButton();

		this.addOrderEvent();

		this.scheduleUpdate();

		return true;
	},

	addBtnBackEvent : function(self) {
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseUp: function(event) {
				if(event.getLocationX() > 45 && event.getLocationX() < 90 
					&& event.getLocationY() > 500 && event.getLocationY() < 540) {
					var stock = new Stock();
					stock.init();
					self.getParent().addChild(stock);
					self.getParent().removeChild(self);
				}
			}
		}, this);
	},

	addOrderEvent : function() {
		var self = this;
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseUp: function(event) {
				if(event.getLocationX() > 215 && event.getLocationX() < 565 
					&& event.getLocationY() > 360 && event.getLocationY() < 420) {
					cup += 50;
					money -= 2.0;
					if(money < 0) {
						self.showPopup();
						cup -= 50;
						money += 2.0;
						isPopupShown = true;
					}
				}

				else if(event.getLocationX() > 215 && event.getLocationX() < 565 
					&& event.getLocationY() > 240 && event.getLocationY() < 300) {
					cup += 100;
					money -= 3.5;
					if(money < 0) {
						self.showPopup();
						cup -= 100;
						money += 3.5;
						isPopupShown = true;
					}
				}

				else if(event.getLocationX() > 215 && event.getLocationX() < 565 
					&& event.getLocationY() > 110 && event.getLocationY() < 170) {
					cup += 200;
					money -= 6.0;
					if(money < 0) {
						self.showPopup();
						cup -= 200;
						money += 6.0;
						isPopupShown = true;
					}
				}
			}
		}, this);
	},

	showPopup : function() {
		var popup = new Popup();
		popup.init();
		this.addChild(popup);
	},

	createMoneyLabel : function() {
		this.moneyLabel = cc.LabelTTF.create(money.toFixed(1).toString() + " $","AG Book Rounded",40);
		this.moneyLabel.setPosition(new cc.Point(700, 550));
		this.addChild(this.moneyLabel);
	},

	createBackButton : function() {
		this.btnBack = new BtnBack();
		this.btnBack.setPosition(new cc.Point(70, 540));
		this.addChild(this.btnBack);
		this.addBtnBackEvent(this);
	},

	update : function() {
		this.moneyLabel.setString(money.toFixed(1).toString() + " $");
	}
});