var BtnStock = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile('res/KL_Stock_Button.png');
	},

	addBtnStockEvent : function(self) {
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseUp: function(event) {
				if(event.getLocationX() > 0 && event.getLocationX() < 300 
					&& event.getLocationY() > 400 && event.getLocationY() < 500) {
						var stock = new Stock();
						stock.init();
						self.getParent().addChild(stock);
						self.getParent().removeChild(self);
					}
				}
		}, this);
	}
});