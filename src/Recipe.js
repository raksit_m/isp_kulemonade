var Recipe = cc.LayerColor.extend({
	init: function() {
		this.setPosition(new cc.Point(0, 0));
		
		this.backGround = new cc.Sprite.create("res/KL_Recipe.png");
		this.backGround.setPosition(new cc.Point(400, 300));
		this.addChild(this.backGround);

		this.lemonRecipe = cc.LabelTTF.create(lemonPerCup.toString(),"AG Book Rounded",40);
		this.lemonRecipe.setPosition(new cc.Point(540, 495));
		this.addChild(this.lemonRecipe);

		this.iceRecipe = cc.LabelTTF.create(icePerCup.toString(),"AG Book Rounded",40);
		this.iceRecipe.setPosition(new cc.Point(540, 395));
		this.addChild(this.iceRecipe);

		this.sugarRecipe = cc.LabelTTF.create(sugarPerCup.toString(),"AG Book Rounded",40);
		this.sugarRecipe.setPosition(new cc.Point(540, 285));
		this.addChild(this.sugarRecipe);

		this.priceRecipe = cc.LabelTTF.create(pricePerCup.toFixed(1).toString(),"AG Book Rounded",40);
		this.priceRecipe.setPosition(new cc.Point(540, 180));
		this.addChild(this.priceRecipe);
	
		this.btnBack = new BtnBack();
		this.btnBack.setPosition(new cc.Point(70, 540));
		this.addChild(this.btnBack);

		this.addCustomizeRecipeEvent();
		this.addBtnBackEvent(this);
		this.scheduleUpdate();

		return true;
	},
	
	addBtnBackEvent : function(self) {
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseUp: function(event) {
				if(event.getLocationX() > 45 && event.getLocationX() < 90 
					&& event.getLocationY() > 500 && event.getLocationY() < 540) {
					var firstPage = new FirstPage();
					firstPage.init();
					self.getParent().addChild(firstPage);
					self.getParent().removeChild(self);
				}
			}
		}, this);
	},

	addCustomizeRecipeEvent: function() {
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseUp: function(event) {
				if(event.getLocationX() > 420 && event.getLocationX() < 440) {

					if(event.getLocationY() > 470 && event.getLocationY() < 510 && lemonPerCup > 1) {					
						lemonPerCup -= 1;
					}

					else if(event.getLocationY() > 370 && event.getLocationY() < 410 && icePerCup > 1) {
						icePerCup -= 1;
					}

					else if(event.getLocationY() > 270 && event.getLocationY() < 310 && sugarPerCup > 1) {
						sugarPerCup -= 1;
					}

					else if(event.getLocationY() > 170 && event.getLocationY() < 210 && pricePerCup > 0.2) {
						pricePerCup -= 0.1;
					}
				}

				else if(event.getLocationX() > 640 && event.getLocationX() < 660) {

					if(event.getLocationY() > 470 && event.getLocationY() < 510 && lemonPerCup < 5) {						
						lemonPerCup += 1;
					}

					else if(event.getLocationY() > 370 && event.getLocationY() < 410 && icePerCup < 10) {
						icePerCup += 1;
					}

					else if(event.getLocationY() > 270 && event.getLocationY() < 310 && sugarPerCup < 5) {
						sugarPerCup += 1;
					}

					else if(event.getLocationY() > 170 && event.getLocationY() < 210 && pricePerCup <= 3) {
						pricePerCup += 0.1;
					}
				}
			}
		}, this);
	},

	update: function() {
		this.lemonRecipe.setString(lemonPerCup.toString());
		this.iceRecipe.setString(icePerCup.toString());
		this.sugarRecipe.setString(sugarPerCup.toString());
		this.priceRecipe.setString(pricePerCup.toFixed(1).toString());
	}
});	
